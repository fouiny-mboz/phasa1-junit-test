package bdd.airport;

public class PremiumFlight extends bdd.airport.Flight {
    public PremiumFlight(String id) {
        super(id);
    }

    @Override
    public boolean addPassenger(bdd.airport.Passenger passenger) {
        if(passenger.isVip()){
            return passengers.add(passenger);
        }
        return false;
    }

    @Override
    public boolean removePassenger(bdd.airport.Passenger passenger) {
        if(passenger.isVip()){
            return passengers.remove(passenger);
        }
        return false;
    }


}
