package bdd.airport;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.sql.Array;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class PassengerPolicy {
    private Flight economyFlight;
    private Flight businessFlight;
    private Flight premiumFlight;
    private Passenger mike;
    private Passenger james;


    @Given("^there is an economy flight$")
    public void thereIsAnEconomyFlight() {
        economyFlight = new EconomyFlight("1");
    }

    @When("^we have a regular passenger$")
    public void weHaveARegularPassenger() {
        mike = new Passenger("Mike", false);
    }

    @Then("^you can add regular passenger and remove him from economy flight$")
    public void youCanAddAndRemoveHimFromEconomyFlight() throws Throwable {
        assertAll("Verify all conditions for a regular passenger and economy flight",
                () -> assertEquals("1", economyFlight.getId()),
                () -> assertEquals(true, economyFlight.addPassenger(mike)),
                () -> assertEquals(1, economyFlight.getPassengersSet().size()),
                ()  -> assertTrue(new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName().equals("Mike")),
                () -> assertTrue(economyFlight.getPassengersSet().contains(mike)),
                () -> assertEquals("Mike", new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName()),
                () -> assertEquals(true, economyFlight.removePassenger(mike)),
                () -> assertEquals(0, economyFlight.getPassengersSet().size())
        );
    }

    @And("^you cannot add a regular passenger to an economy flight more than once$")
    public void youCannotAddARegularPassengerToAnEconomyFlightMoreThanOnce() throws Throwable{
        for (int i = 0; i < 10; i++) {
            economyFlight.addPassenger(mike);
        }
        assertAll("Verify a regular passenger ca be added an economy flight only once",
                () -> assertEquals("1", economyFlight.getId()),
                () -> assertEquals(1, economyFlight.getPassengersSet().size()),
                () -> assertTrue(economyFlight.getPassengersSet().contains(mike)),
                ()  -> assertTrue(new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName().equals("Mike")),
                () -> assertEquals("Mike", new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName()),
                () -> assertEquals(true, economyFlight.removePassenger(mike)),
                () -> assertEquals(0, economyFlight.getPassengersSet().size())
        );
    }

    @Given("^there is a business flight$")
    public void thereIsABusinessFlight() {
        businessFlight = new BusinessFlight("2");
    }

    @Then("^you cannot add a regular passenger or remove him from a business flight$")
    public void youCannotAddOrRemoveHimFromABusiness()  throws Throwable{
        assertAll("Verify all conditions for a regular passenger and business flight",
                () -> assertEquals("2", businessFlight.getId()),
                () -> assertEquals(false, businessFlight.addPassenger(mike)),
                () -> assertEquals(0, businessFlight.getPassengersSet().size()),
                () -> assertEquals(false, businessFlight.removePassenger(mike)),
                () -> assertEquals(0, businessFlight.getPassengersSet().size())
        );
    }

    @Given("^there is a premium flight$")
    public void thereIsAPremiumFlight() {
        premiumFlight = new PremiumFlight("3");
    }


    @Then("^you cannot add regular passenger or remove him from premium flight$")
    public void youCannotAddOrRemoveHimFromPremiumFlight() throws Throwable {
        assertAll("Verify all conditions for regular and premium flight",
                ()  -> assertEquals("3", premiumFlight.getId()),
                ()  -> assertEquals(false,premiumFlight.addPassenger(mike)),
                ()  -> assertEquals(0, premiumFlight.getPassengersSet().size()),
                ()  -> assertEquals(false, premiumFlight.removePassenger(mike)),
                ()  -> assertEquals(0, premiumFlight.getPassengersSet().size())
        );
    }



    @When("^we have a VIP passenger$")
    public void weHaveAVIPPassenger() {
        james = new Passenger("James", true);
    }

    @Then("^you can add a VIP passenger but cannot remove him for economy flight$")
    public void youCanAddHimButCannotRemoveHimForEconomyFlight() throws Throwable {
        assertAll("Verify all conditions for a VIP passenger and economy flight",
                () -> assertEquals("1", economyFlight.getId()),
                () -> assertEquals(true, economyFlight.addPassenger(james)),
                () -> assertEquals(1, economyFlight.getPassengersSet().size()),
                () -> assertEquals("James", new ArrayList<>(economyFlight.getPassengersSet()).get(0).getName()),
                () -> assertEquals(false, economyFlight.removePassenger(james)),
                () -> assertEquals(1, economyFlight.getPassengersSet().size())
        );
    }

    @And("^you cannot add VIP passenger to an economy flight more than once$")
    public void youCannotAddVIPPassengerToAnEconomyFlightMoreThanOnce() throws Throwable{
        for (int i = 0; i < 10; i++) {
            economyFlight.addPassenger(james);
        }

        assertAll("Verify a VIP passenger can be added  to an economy flight only once",
                () -> assertEquals("1", economyFlight.getId()),
                () -> assertEquals(1, economyFlight.getPassengersSet().size()),
                () -> assertTrue(economyFlight.getPassengersSet().contains(james)),
                () -> assertEquals(false, economyFlight.removePassenger(james)),
                () -> assertEquals(1, economyFlight.getPassengersSet().size())
        );
    }


    @Then("^you can add VIP passenger but cannot  remove him from business flight$")
    public void youCanAddButCannotRemoveHimFromBusinessFlight() throws Throwable{
        assertAll("Verify all conditions for VIP passenger and business flight",
                ()  -> assertEquals("2", businessFlight.getId()),
                ()  -> assertEquals(true, businessFlight.addPassenger(james)),
                ()  -> assertEquals(1, businessFlight.getPassengersSet().size()),
                ()  -> assertTrue(businessFlight.getPassengersSet().contains(james)),
                ()  -> assertEquals(false, businessFlight.removePassenger(james)),
                ()  -> assertEquals(1, businessFlight.getPassengersSet().size())
        );
    }


    @Then("^you can add VIP passenger  or remove him from premium flight$")
    public void youCanAddVIPPassengerOrRemoveHimFromPremiumFlight() throws Throwable{
        assertAll("Verify all conditions for VIP passenger",
                ()  -> assertEquals("3", premiumFlight.getId()),
                ()  -> assertEquals(true, premiumFlight.addPassenger(james)),
                ()  -> assertEquals(1, premiumFlight.getPassengersSet().size()),
                ()  -> assertTrue(new ArrayList<>(premiumFlight.getPassengersSet()).get(0).getName().equals("James")),
                ()  -> assertEquals("James", new ArrayList<>(premiumFlight.getPassengersSet()).get(0).getName()),
                ()  -> assertEquals(true, premiumFlight.removePassenger(james)),
                ()  -> assertEquals(0, premiumFlight.getPassengersSet().size())
                );
    }
    @And("^you cannot add a VIP passenger to an premium flight more than once$")
    public void youCannotAddAVIPPassengerToAnPremiumFlightMoreThanOnce() throws Throwable {
        for(int i=0; i < 10; i++){
            premiumFlight.addPassenger(james);
        }
        assertAll("Verify a VIP passenger can be added premium flight only once",
                ()  -> assertEquals("3", premiumFlight.getId()),
                ()  -> assertEquals(1, premiumFlight.getPassengersSet().size()),
                ()  -> assertTrue(premiumFlight.getPassengersSet().contains(james)),
                ()  -> assertEquals(true, premiumFlight.removePassenger(james)),
                ()  -> assertEquals(0, premiumFlight.getPassengersSet().size())
        );
    }
}
