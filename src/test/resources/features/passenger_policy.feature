Feature: Passengers Policy
  The company follows a policy of and removing passengers,
  depending on the passenger type and on the flight type

  Scenario: Economy flight, regular passenger
    Given there is an economy flight
    When we have a regular passenger
    Then you can add regular passenger and remove him from economy flight
    And you cannot add a regular passenger to an economy flight more than once



  Scenario: Economy flight, VIP passenger
    Given there is an economy flight
    When we have a VIP passenger
    Then you can add a VIP passenger but cannot remove him for economy flight
    And you cannot add VIP passenger to an economy flight more than once


  Scenario: Business flight , regular passenger
    Given there is a business flight
    When we have a regular passenger
    Then you cannot add a regular passenger or remove him from a business flight

  Scenario: Business flight, VIP passenger
    Given there is a business flight
    When we have a VIP passenger
    Then you can add VIP passenger but cannot  remove him from business flight

  Scenario: Premium flight, regular passenger
    Given there is a premium flight
    When we have a regular passenger
    Then you cannot add regular passenger or remove him from premium flight

  Scenario: Premium flight, VIP passenger
    Given there is a premium flight
    When we have a VIP passenger
    Then you can add VIP passenger  or remove him from premium flight
    And you cannot add a VIP passenger to an premium flight more than once


